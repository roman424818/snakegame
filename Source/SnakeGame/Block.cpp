// Fill out your copyright notice in the Description page of Project Settings.


#include "Block.h"
#include "SnakeBase.h"

// Sets default values
ABlock::ABlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	NumElement = 5;
	OffsetElement = 100.f;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	Element = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Element"));
	Element->SetupAttachment(SceneComponent);
}

void ABlock::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (BuiltOffsetElement == OffsetElement && BuiltNumElement == NumElement)
		return;

	BuiltNumElement = NumElement;
	BuiltOffsetElement = OffsetElement;

	Element->ClearInstances();
	FVector Translation(0.f, 0.f, 0.f);

	for (uint32 a = 0; a < NumElement; a++)
	{
		Translation.Y = OffsetElement * a;
		Element->AddInstance(FTransform(Translation));
	}
}

// Called when the game starts or when spawned
void ABlock::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABlock::InteractBlock(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Yellow, TEXT("Call from InteractBlock!"));
			Snake->RestartGame();
		}
	}
}

