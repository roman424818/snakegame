// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Block.generated.h"

UCLASS()
class SNAKEGAME_API ABlock: public AActor, public IInteractable
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ABlock();

	virtual void OnConstruction(const FTransform& Transform) override;

	UPROPERTY(EditAnywhere)
	class UInstancedStaticMeshComponent* Element;

	UPROPERTY(EditAnywhere, meta = (ClampMin = 0, ClampMax = 20))
	uint32 NumElement;

	UPROPERTY(EditAnywhere, meta = (ClampMin = 0))
	float OffsetElement;

private:

	uint32 BuiltNumElement;
	uint32 BuiltOffsetElement;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void InteractBlock(AActor* Interactor, bool bIsHead) override;
};
