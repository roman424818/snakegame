// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodAddSpeed.h"
#include "SnakeBase.h"

// Sets default values
AFoodAddSpeed::AFoodAddSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodAddSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodAddSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodAddSpeed::InteractAddSpeed(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Orange, TEXT("Call from InteractAddSpeed!"));
			Snake->SetActorTickInterval(0.1f);
			Snake->PlaySoundEat();
			Destroy();
		}
	}
}

