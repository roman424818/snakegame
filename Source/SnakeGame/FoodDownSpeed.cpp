// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodDownSpeed.h"
#include "SnakeBase.h"

// Sets default values
AFoodDownSpeed::AFoodDownSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodDownSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodDownSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodDownSpeed::InteractDownSpeed(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Orange, TEXT("Call from InteractDownSpeed!"));
			Snake->SetActorTickInterval(0.4f);
			Snake->PlaySoundEat();
			Destroy();
		}
	}
}

