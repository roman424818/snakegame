// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodRemoveSnakeElement.h"
#include "SnakeBase.h"

// Sets default values
AFoodRemoveSnakeElement::AFoodRemoveSnakeElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodRemoveSnakeElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodRemoveSnakeElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodRemoveSnakeElement::InteractRemoveSnakeElement(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Orange, TEXT("Call from InteractRemoveFood!"));
			Snake->RemoveSnakeElement();
			Snake->PlaySoundEat();
			Destroy();
		}
	}
}

