// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"

// Add default functionality here for any IInteractable functions that are not pure virtual.

void IInteractable::Interact(AActor* Interactor, bool bIsHead)
{

}

void IInteractable::InteractBlock(AActor* Interactor, bool bIsHead)
{

}

void IInteractable::InteractRemoveSnakeElement(AActor* Interactor, bool bIsHead)
{

}

void IInteractable::InteractAddSpeed(AActor* Interactor, bool bIshead)
{

}

void IInteractable::InteractDownSpeed(AActor* Interactor, bool bIsHead)
{

}
