// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interactable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SNAKEGAME_API IInteractable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void Interact(AActor* Interactor, bool bIsHead);
	virtual void InteractBlock(AActor* Interactor, bool bIsHead);
	virtual void InteractRemoveSnakeElement(AActor* Interactor, bool bIsHead);
	virtual void InteractAddSpeed(AActor* Interactor, bool bIshead);
	virtual void InteractDownSpeed(AActor* Interactor, bool bIsHead);
};
