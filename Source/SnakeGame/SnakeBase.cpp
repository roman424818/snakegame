// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
	MovementSpeed = 10.f;

	static ConstructorHelpers::FObjectFinder<USoundCue> DeathSoundCueObject(TEXT("SoundCue'/Game/audio/DeathSoundCue.DeathSoundCue'"));

	DeathSoundCue = DeathSoundCueObject.Object;
	DeathAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("DeathAudioComponent"));
	DeathAudioComponent->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<USoundCue> EatSoundCueObject(TEXT("SoundCue'/Game/audio/EatSoundCue.EatSoundCue'"));

	EatSoundCue = EatSoundCueObject.Object;
	EatAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("EatAudioComponent"));
	EatAudioComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);

	GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeGameOnly());
	GetWorld()->GetFirstPlayerController()->SetShowMouseCursor(false);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);

		if (SnakeElements.Num() > 0)
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		}
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::RemoveSnakeElement()
{
	for (int i = SnakeElements.Num() - 1; i > 1; i--)
	{
		SnakeElements[SnakeElements.Num() - 1]->Destroy();
	}
	if (SnakeElements.Num() - 1 > 1)
	{
		SnakeElements.RemoveAt(SnakeElements.Num() - 1);
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	Moving = false;
}

void ASnakeBase::RotateHead()
{
	FRotator NewRotation(FRotator::ZeroRotator);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		NewRotation.Yaw += 180.f;
		break;
	case EMovementDirection::DOWN:
		NewRotation.Yaw -= 0.f;
		break;
	case EMovementDirection::LEFT:
		NewRotation.Yaw = -90.f;
		break;
	case EMovementDirection::RIGHT:
		NewRotation.Yaw = 90.f;
		break;
	}

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FRotator PrevLocation = PrevElement->GetActorRotation();
		CurrentElement->SetActorRotation(PrevLocation);
	}

	SnakeElements[0]->SetActorRelativeRotation(NewRotation);
}

void ASnakeBase::RestartGame()
{
	GetWorld()->GetFirstPlayerController()->Pause();
	UUserWidget* WdgRetry = CreateWidget<UUserWidget>(GetGameInstance(), WidgetClass);
	WdgRetry->AddToViewport();
	WdgRetry->PlaySound(DeathSoundCue);

	GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeUIOnly());
	GetWorld()->GetFirstPlayerController()->SetShowMouseCursor(true);
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
			InteractableInterface->InteractBlock(this, bIsFirst);
			InteractableInterface->InteractRemoveSnakeElement(this, bIsFirst);
			InteractableInterface->InteractAddSpeed(this, bIsFirst);
			InteractableInterface->InteractDownSpeed(this, bIsFirst);
		}
	}
}

void ASnakeBase::PlaySoundEat()
{
	if (EatAudioComponent && EatSoundCue)
	{
		EatAudioComponent->SetSound(EatSoundCue);
		EatAudioComponent->Play();
	}
}
